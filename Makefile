PROJECT=message-broker-bundle

up:
	docker-compose -f infrastructure/dev/docker-compose.yml -p $(PROJECT) up -d
down:
	docker-compose -f infrastructure/dev/docker-compose.yml -p $(PROJECT) down
test:
	docker-compose -f infrastructure/dev/docker-compose.yml -p $(PROJECT) exec php-cli vendor/bin/phpunit
cs:
	docker-compose -f infrastructure/dev/docker-compose.yml -p $(PROJECT) exec php-cli vendor/bin/phpcs --standard=PSR12 src tests
cs-fix:
	docker-compose -f infrastructure/dev/docker-compose.yml -p $(PROJECT) exec php-cli vendor/bin/phpcbf --standard=PSR12 src tests
deps:
	docker run -v $(shell pwd):/app --rm -t composer install --ignore-platform-reqs
