# Message Broker Bundle

This is a Symfony bundle used for integrating talentrydev/message-broker library into a Symfony project.

## Installing

* Run:

```
composer require talentrydev/message-broker-bundle
```

* Add the MessageBrokerBundle to your kernel's `registerBundles` method:

```
return [
    //...
    new \Talentry\MessageBrokerBundle\MessageBrokerBundle();
];
```

## Configuring

Bundle exposes the following configuration:

| Option                    | Type   | Default value | Available values      | Required                                     | Description                                                                       |
|---------------------------|--------|---------------|-----------------------|----------------------------------------------|-----------------------------------------------------------------------------------|
| aws_region                | string | eu-central-1  | Any valid AWS region  | only when using sns or sqs implementation    | AWS region                                                                        |
| channels                  | array  | -             | See below             | no                                           | Used for configuring channels and serialization strategies                        |
| kafka                     | array  | -             | See below             | no                                           | Used for configuring Kafka                                                        |
| namespace                 | string | -             | Any string            | no                                           | Used for prefixing SQS queue names                                                |
| publisher_implementation  | string | sqs           | sqs,sns,array,kafka   | yes                                          | Selects the message broker implementation used for publishing messages            |
| sns_endpoint              | string | -             | Any valid URL         | no                                           | Allows using a custom AWS SNS implementation (eg. localstack); useful for testing |
| sns_topic_arn             | string | -             | Any valid AWS SNS ARN | only when using sns publisher implementation | ARN of the SNS topic to which messages will be published                          | 
| sqs_endpoint              | string | -             | Any valid URL         | no                                           | Allows using a custom AWS SQS implementation (eg. localstack); useful for testing | 
| subscriber_implementation | string | sqs           | sqs,array,kafka       | yes                                          | Selects the message broker implementation used for receiving messages             | 

Channels configuration:

| Option                 | Type      | Default value | Available values                        | Required | Description                                                 |
|------------------------|-----------|---------------|-----------------------------------------|----------|-------------------------------------------------------------|
| name                   | string    | -             | Any valid channel name                  | yes      | Name of the channel                                         |
| serialization_strategy | string    | -             | Valid serialization strategy class name | yes      | FQCN of the class implementing MessageSerializationStrategy |


Kafka configuration:

| Option               | Type      | Default value | Available values         | Required                               | Description                                                                  |
|----------------------|-----------|---------------|--------------------------|----------------------------------------|------------------------------------------------------------------------------|
| auto_offset_reset    | string    | earliest      | latest, earliest, none   | only when using kafka implementation   | https://kafka.apache.org/documentation/#consumerconfigs_auto.offset.reset    |
| broker_list          | string    | -             | Valid list of host names | only when using kafka implementation   | https://arnaud.le-blanc.net/php-rdkafka-doc/phpdoc/rdkafka.addbrokers.html   |
| enable_partition_eof | string    | true          | true or false            | only when using kafka implementation   | https://docs.confluent.io/3.2.1/clients/librdkafka/CONFIGURATION_8md.html    |
| enable_auto_commit   | string    | false         | true or false            | only when using kafka implementation   | https://kafka.apache.org/documentation/#consumerconfigs_enable.auto.commit   |
| log_level            | integer   | 3             | Any LOG_* PHP constant   | only when using kafka implementation   | Sets the log level of the Kafka PHP module                                   |
| max_poll_interval_ms | integer   | 300000        | Any integer              | only when using kafka implementation   | https://kafka.apache.org/documentation/#consumerconfigs_max.poll.interval.ms |
| session_timeout_ms   | integer   | 45000         | Any integer              | only when using kafka implementation   | https://kafka.apache.org/documentation/#connectconfigs_session.timeout.ms    |


## Important considerations when using kafka implementation

- In order to use the kafka implementation, you need to install the php-rdkafka extension: https://arnaud.le-blanc.net/php-rdkafka-doc/phpdoc/index.html
- There is no support for dead letter queues
- Horizontal scaling of the consumers is rather limited: the number of consumers cannot exceed the number of partitions on the given topic
- If the processing of the message takes longer than the [`max.poll.interval.ms`](https://docs.confluent.io/platform/current/installation/configuration/consumer-configs.html#max-poll-interval-ms),
the consumer will be kicked out of the group and won't receive any more messages
