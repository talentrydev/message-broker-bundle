<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerBundle\Tests\DependencyInjection;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Talentry\MessageBroker\ApplicationInterface\MessagePublisher;
use Talentry\MessageBroker\ApplicationInterface\MessageSubscriber;
use Talentry\MessageBroker\Domain\Channel\Channel;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\RedisFailedMessageSpool;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\VoidFailedMessageSpool;
use Talentry\MessageBroker\Infrastructure\MessageBrokerFactory;
use Talentry\MessageBroker\Infrastructure\Serialization\Base64PhpSerializationStrategy;
use Talentry\MessageBroker\Infrastructure\Serialization\PhpSerializationStrategy;
use Talentry\MessageBroker\Tests\PrivatePropertyAccess;

class ConfigurationTest extends KernelTestCase
{
    private MessagePublisher $publisher;
    private MessageSubscriber $subscriber;

    public function testWithDefaultConfiguration(): void
    {
        $this->givenEnvironment('default');
        $this->expectPublisherImplementationIsConfigured('sqs');
        $this->expectSubscriberImplementationIsConfigured('sqs');
        $this->expectAwsRegionIsConfiguredOnSqsPublisher('eu-central-1');
        $this->expectAwsRegionIsConfiguredOnSqsSubscriber('eu-central-1');
        $this->expectNamespaceIsConfiguredOnSqsPublisher(null);
        $this->expectNamespaceIsConfiguredOnSqsSubscriber(null);
        $this->expectEndpointIsConfiguredOnSqsPublisher('https://sqs.eu-central-1.amazonaws.com');
        $this->expectEndpointIsConfiguredOnSqsSubscriber('https://sqs.eu-central-1.amazonaws.com');
        $this->expectFailedMessageSpoolIsConfigured(VoidFailedMessageSpool::class);
    }

    public function testWithRegionConfiguration(): void
    {
        $this->givenEnvironment('region');
        $this->expectPublisherImplementationIsConfigured('sqs');
        $this->expectSubscriberImplementationIsConfigured('sqs');
        $this->expectAwsRegionIsConfiguredOnSqsPublisher('us-east-1');
        $this->expectAwsRegionIsConfiguredOnSqsSubscriber('us-east-1');
        $this->expectNamespaceIsConfiguredOnSqsPublisher(null);
        $this->expectNamespaceIsConfiguredOnSqsSubscriber(null);
        $this->expectEndpointIsConfiguredOnSqsPublisher('https://sqs.us-east-1.amazonaws.com');
        $this->expectEndpointIsConfiguredOnSqsSubscriber('https://sqs.us-east-1.amazonaws.com');
        $this->expectFailedMessageSpoolIsConfigured(VoidFailedMessageSpool::class);
    }

    public function testWithNamespaceConfiguration(): void
    {
        $this->givenEnvironment('namespace');
        $this->expectPublisherImplementationIsConfigured('sqs');
        $this->expectSubscriberImplementationIsConfigured('sqs');
        $this->expectAwsRegionIsConfiguredOnSqsPublisher('eu-central-1');
        $this->expectAwsRegionIsConfiguredOnSqsSubscriber('eu-central-1');
        $this->expectNamespaceIsConfiguredOnSqsPublisher('prod');
        $this->expectNamespaceIsConfiguredOnSqsSubscriber('prod');
        $this->expectEndpointIsConfiguredOnSqsPublisher('https://sqs.eu-central-1.amazonaws.com');
        $this->expectEndpointIsConfiguredOnSqsSubscriber('https://sqs.eu-central-1.amazonaws.com');
        $this->expectFailedMessageSpoolIsConfigured(VoidFailedMessageSpool::class);
    }

    public function testWithArrayBrokerConfiguration(): void
    {
        $this->givenEnvironment('array_broker');
        $this->expectPublisherImplementationIsConfigured('array');
        $this->expectSubscriberImplementationIsConfigured('array');
        $this->expectFailedMessageSpoolIsConfigured(VoidFailedMessageSpool::class);
    }

    public function testWithSnsPublisherWithTopicArnConfiguration(): void
    {
        $this->givenEnvironment('sns_publisher_with_topic_arn');
        $this->expectPublisherImplementationIsConfigured('sns');
        $this->expectSubscriberImplementationIsConfigured('sqs');
        $this->expectAwsRegionIsConfiguredOnSnsPublisher('eu-central-1');
        $this->expectAwsRegionIsConfiguredOnSqsSubscriber('eu-central-1');
        $this->expectNamespaceIsConfiguredOnSqsSubscriber(null);
        $this->expectEndpointIsConfiguredOnSnsPublisher('https://sns.eu-central-1.amazonaws.com');
        $this->expectEndpointIsConfiguredOnSqsSubscriber('https://sqs.eu-central-1.amazonaws.com');
        $this->expectSnsTopicArnIsConfiguredOnSnsPublisher('arn:aws:sns:eu-central-1:000000000000:topic');
        $this->expectFailedMessageSpoolIsConfigured(VoidFailedMessageSpool::class);
    }

    public function testWithSnsEndpointConfiguration(): void
    {
        $this->givenEnvironment('sns_endpoint');
        $this->expectPublisherImplementationIsConfigured('sns');
        $this->expectSubscriberImplementationIsConfigured('sqs');
        $this->expectAwsRegionIsConfiguredOnSnsPublisher('eu-central-1');
        $this->expectAwsRegionIsConfiguredOnSqsSubscriber('eu-central-1');
        $this->expectNamespaceIsConfiguredOnSqsSubscriber(null);
        $this->expectEndpointIsConfiguredOnSnsPublisher('http://localhost:4566');
        $this->expectEndpointIsConfiguredOnSqsSubscriber('https://sqs.eu-central-1.amazonaws.com');
        $this->expectFailedMessageSpoolIsConfigured(VoidFailedMessageSpool::class);
    }

    public function testWithSqsEndpointConfiguration(): void
    {
        $this->givenEnvironment('sqs_endpoint');
        $this->expectPublisherImplementationIsConfigured('sqs');
        $this->expectSubscriberImplementationIsConfigured('sqs');
        $this->expectAwsRegionIsConfiguredOnSqsPublisher('eu-central-1');
        $this->expectAwsRegionIsConfiguredOnSqsSubscriber('eu-central-1');
        $this->expectNamespaceIsConfiguredOnSqsPublisher(null);
        $this->expectNamespaceIsConfiguredOnSqsSubscriber(null);
        $this->expectEndpointIsConfiguredOnSqsPublisher('http://localhost:4566');
        $this->expectEndpointIsConfiguredOnSqsSubscriber('http://localhost:4566');
        $this->expectFailedMessageSpoolIsConfigured(VoidFailedMessageSpool::class);
    }

    public function testWithRedisConfiguration(): void
    {
        $this->givenEnvironment('redis');
        $this->expectPublisherImplementationIsConfigured('sqs');
        $this->expectSubscriberImplementationIsConfigured('sqs');
        $this->expectAwsRegionIsConfiguredOnSqsPublisher('eu-central-1');
        $this->expectAwsRegionIsConfiguredOnSqsSubscriber('eu-central-1');
        $this->expectNamespaceIsConfiguredOnSqsPublisher(null);
        $this->expectNamespaceIsConfiguredOnSqsSubscriber(null);
        $this->expectEndpointIsConfiguredOnSqsPublisher('https://sqs.eu-central-1.amazonaws.com');
        $this->expectEndpointIsConfiguredOnSqsSubscriber('https://sqs.eu-central-1.amazonaws.com');
        $this->expectFailedMessageSpoolIsConfigured(RedisFailedMessageSpool::class);
    }

    public function testWithKafkaConfiguration(): void
    {
        $this->givenEnvironment('kafka');
        $this->expectPublisherImplementationIsConfigured('kafka');
        $this->expectSubscriberImplementationIsConfigured('kafka');
        $this->expectBrokerListIsConfiguredOnKafkaPublisher('localhost:9092');
        $this->expectBrokerListIsConfiguredOnKafkaSubscriber('localhost:9092');
        $this->expectLogLevelIsConfiguredOnKafkaPublisher(LOG_EMERG);
        $this->expectLogLevelIsConfiguredOnKafkaSubscriber(LOG_EMERG);
        $this->expectMaxPollIntervalIsConfiguredOnKafkaPublisher(1000);
        $this->expectMaxPollIntervalIsConfiguredOnKafkaSubscriber(1000);
        $this->expectFailedMessageSpoolIsConfigured(VoidFailedMessageSpool::class);
    }

    public function testWithChannelNamesConfiguration(): void
    {
        $this->givenEnvironment('channel_names');
        $this->expectMessageSerializationStrategiesAreConfigured();
    }

    public function testWithArrayInEnvVarsConfiguration(): void
    {
        $this->givenEnvVar('PUBLISHER_IMPLEMENTATION', 'array');
        $this->givenEnvVar('SUBSCRIBER_IMPLEMENTATION', 'array');
        $this->givenEnvironment('env_vars');
        $this->expectPublisherImplementationIsConfigured('array');
        $this->expectSubscriberImplementationIsConfigured('array');
        $this->expectFailedMessageSpoolIsConfigured(VoidFailedMessageSpool::class);
    }

    public function testWithSqsInEnvVarsConfiguration(): void
    {
        $this->givenEnvVar('PUBLISHER_IMPLEMENTATION', 'sqs');
        $this->givenEnvVar('SUBSCRIBER_IMPLEMENTATION', 'sqs');
        $this->givenEnvVar('NAMESPACE', 'ns');
        $this->givenEnvironment('env_vars');
        $this->expectPublisherImplementationIsConfigured('sqs');
        $this->expectSubscriberImplementationIsConfigured('sqs');
        $this->expectNamespaceIsConfiguredOnSqsPublisher('ns');
        $this->expectNamespaceIsConfiguredOnSqsSubscriber('ns');
        $this->expectFailedMessageSpoolIsConfigured(VoidFailedMessageSpool::class);
    }

    private function givenEnvironment(string $environment): void
    {
        self::bootKernel(['environment' => $environment]);
        /** @var MessageBrokerFactory $factory */
        $factory = self::getContainer()->get(MessageBrokerFactory::class);
        $this->publisher = $factory->generateMessagePublisher();
        $this->subscriber = $factory->generateMessageSubscriber();
    }

    private function givenEnvVar(string $name, string $value): void
    {
        putenv("$name=$value");
    }

    private function expectAwsRegionIsConfiguredOnSqsPublisher(string $region): void
    {
        $sqsClient = PrivatePropertyAccess::getPrivatePropertyValue($this->publisher, 'sqsClient');
        self::assertSame($region, $sqsClient->getRegion());
    }

    private function expectAwsRegionIsConfiguredOnSqsSubscriber(string $region): void
    {
        $sqsClient = PrivatePropertyAccess::getPrivatePropertyValue($this->subscriber, 'sqsClient');
        self::assertSame($region, $sqsClient->getRegion());
    }

    private function expectAwsRegionIsConfiguredOnSnsPublisher(string $region): void
    {
        $snsClient = PrivatePropertyAccess::getPrivatePropertyValue($this->publisher, 'snsClient');
        self::assertSame($region, $snsClient->getRegion());
    }

    private function expectNamespaceIsConfiguredOnSqsPublisher(?string $namespace = null): void
    {
        $sqsQueueFactory = PrivatePropertyAccess::getPrivatePropertyValue($this->publisher, 'sqsQueueFactory');
        self::assertSame($namespace, PrivatePropertyAccess::getPrivatePropertyValue($sqsQueueFactory, 'namespace'));
    }

    private function expectNamespaceIsConfiguredOnSqsSubscriber(?string $namespace = null): void
    {
        $sqsQueueFactory = PrivatePropertyAccess::getPrivatePropertyValue($this->subscriber, 'sqsQueueFactory');
        self::assertSame($namespace, PrivatePropertyAccess::getPrivatePropertyValue($sqsQueueFactory, 'namespace'));
    }

    private function expectPublisherImplementationIsConfigured(string $implementation): void
    {
        self::assertSame($implementation, $this->publisher->name());
    }

    private function expectSubscriberImplementationIsConfigured(string $implementation): void
    {
        self::assertSame($implementation, $this->subscriber->name());
    }

    private function expectFailedMessageSpoolIsConfigured(string $class): void
    {
        self::assertSame($class, get_class($this->publisher->getFailedMessageSpool()));
    }

    private function expectEndpointIsConfiguredOnSnsPublisher(string $endpoint): void
    {
        $snsClient = PrivatePropertyAccess::getPrivatePropertyValue($this->publisher, 'snsClient');
        self::assertSame($endpoint, (string) $snsClient->getEndpoint());
    }

    private function expectEndpointIsConfiguredOnSqsPublisher(string $endpoint): void
    {
        $sqsClient = PrivatePropertyAccess::getPrivatePropertyValue($this->publisher, 'sqsClient');
        self::assertSame($endpoint, (string) $sqsClient->getEndpoint());
    }

    private function expectEndpointIsConfiguredOnSqsSubscriber(string $endpoint): void
    {
        $sqsClient = PrivatePropertyAccess::getPrivatePropertyValue($this->subscriber, 'sqsClient');
        self::assertSame($endpoint, (string) $sqsClient->getEndpoint());
    }

    private function expectSnsTopicArnIsConfiguredOnSnsPublisher(string $snsTopicArn): void
    {
        self::assertSame($snsTopicArn, PrivatePropertyAccess::getPrivatePropertyValue($this->publisher, 'snsTopicArn'));
    }

    private function expectBrokerListIsConfiguredOnKafkaPublisher(string $brokerList): void
    {
        $kafkaFactory = PrivatePropertyAccess::getPrivatePropertyValue($this->publisher, 'kafkaFactory');
        self::assertSame($brokerList, PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'brokerList'));
    }

    private function expectBrokerListIsConfiguredOnKafkaSubscriber(string $brokerList): void
    {
        $kafkaFactory = PrivatePropertyAccess::getPrivatePropertyValue($this->subscriber, 'kafkaFactory');
        self::assertSame($brokerList, PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'brokerList'));
    }

    private function expectLogLevelIsConfiguredOnKafkaPublisher(int $logLevel): void
    {
        $kafkaFactory = PrivatePropertyAccess::getPrivatePropertyValue($this->publisher, 'kafkaFactory');
        self::assertSame($logLevel, (int) PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'logLevel'));
    }

    private function expectLogLevelIsConfiguredOnKafkaSubscriber(int $logLevel): void
    {
        $kafkaFactory = PrivatePropertyAccess::getPrivatePropertyValue($this->subscriber, 'kafkaFactory');
        self::assertSame($logLevel, (int) PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'logLevel'));
    }

    private function expectMaxPollIntervalIsConfiguredOnKafkaPublisher(int $maxPollInterval): void
    {
        $kafkaFactory = PrivatePropertyAccess::getPrivatePropertyValue($this->publisher, 'kafkaFactory');
        self::assertSame(
            $maxPollInterval,
            (int) PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'maxPollIntervalMs'),
        );
    }

    private function expectMaxPollIntervalIsConfiguredOnKafkaSubscriber(int $maxPollInterval): void
    {
        $kafkaFactory = PrivatePropertyAccess::getPrivatePropertyValue($this->subscriber, 'kafkaFactory');
        self::assertSame(
            $maxPollInterval,
            (int) PrivatePropertyAccess::getPrivatePropertyValue($kafkaFactory, 'maxPollIntervalMs'),
        );
    }

    private function expectMessageSerializationStrategiesAreConfigured(): void
    {
        /** @var ChannelRegistry $registry */
        $registry = self::getContainer()->get(ChannelRegistry::class);
        $strategy = $registry->getSerializationStrategy(new Channel('EVENTS'));
        self::assertInstanceOf(PhpSerializationStrategy::class, $strategy);
        $strategy = $registry->getSerializationStrategy(new Channel('WEBHOOKS'));
        self::assertInstanceOf(Base64PhpSerializationStrategy::class, $strategy);
    }
}
