<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    private const string ROOT_NODE = 'message_broker';

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(self::ROOT_NODE);

        $treeBuilder
            ->getRootNode()
            ->children()
                ->scalarNode('aws_region')->defaultValue('eu-central-1')->end()
                ->arrayNode('kafka')
                    ->children()
                        ->scalarNode('auto_offset_reset')->defaultNull()->end()
                        ->scalarNode('broker_list')->isRequired()->end()
                        ->scalarNode('enable_partition_eof')->defaultNull()->end()
                        ->scalarNode('enable_auto_commit')->defaultNull()->end()
                        ->integerNode('log_level')->defaultNull()->end()
                        ->integerNode('max_poll_interval_ms')->defaultNull()->end()
                        ->integerNode('session_timeout_ms')->defaultNull()->end()
                    ->end()
                ->end()
                ->scalarNode('namespace')->defaultNull()->end()
                ->scalarNode('publisher_implementation')->defaultValue('sqs')->end()
                ->arrayNode('channels')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->isRequired()->end()
                            ->scalarNode('serialization_strategy')->isRequired()->end()
                        ->end()
                    ->end()
                ->end()
                ->scalarNode('sns_endpoint')->defaultNull()->end()
                ->scalarNode('sns_topic_arn')->defaultNull()->end() //required only publisher_implementation is sns
                ->scalarNode('sqs_endpoint')->defaultNull()->end()
                ->scalarNode('subscriber_implementation')->defaultValue('sqs')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
