<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerBundle\DependencyInjection\Compiler;

use InvalidArgumentException;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Talentry\MessageBroker\Domain\Channel\ChannelRegistry;

class RegisterChannelsCompilerPass implements CompilerPassInterface
{
    public const string TAG_MESSAGE_BROKER_SERIALIZATION_STRATEGY = 'messageBroker.serializationStrategy';
    public const string PARAMETER_CHANNELS_CONFIGURATION = 'messageBroker.channels';

    public function process(ContainerBuilder $container): void
    {
        $channelRegistry = $container->findDefinition(ChannelRegistry::class);

        $serializationStrategies = [];
        foreach ($container->findTaggedServiceIds(self::TAG_MESSAGE_BROKER_SERIALIZATION_STRATEGY) as $id => $tags) {
            $definition = $container->getDefinition($id);
            $serializationStrategies[$id] = $definition;
        }

        if ($container->hasParameter(self::PARAMETER_CHANNELS_CONFIGURATION)) {
            $channels = $container->getParameter(self::PARAMETER_CHANNELS_CONFIGURATION);
            if (!is_array($channels)) {
                throw new InvalidArgumentException('List of channels must be an array');
            }

            foreach ($channels as $channel) {
                $serializationStrategy = $channel['serialization_strategy'];
                if (!array_key_exists($serializationStrategy, $serializationStrategies)) {
                    throw new InvalidArgumentException('Invalid serialization strategy: ' . $serializationStrategy);
                }
                $channelRegistry->addMethodCall('registerChannel', [
                    $channel['name'],
                    new Reference($serializationStrategy),
                ]);
            }
            $container->setParameter(self::PARAMETER_CHANNELS_CONFIGURATION, null);
        }
    }
}
