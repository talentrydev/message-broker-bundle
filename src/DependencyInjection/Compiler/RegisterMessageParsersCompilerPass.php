<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Talentry\MessageBroker\Infrastructure\MessageParser\CompositeMessageParser;

class RegisterMessageParsersCompilerPass implements CompilerPassInterface
{
    public const string TAG_MESSAGE_BROKER_MESSAGE_PARSER = 'messageBroker.messageParser';

    public function process(ContainerBuilder $container): void
    {
        $compositeParser = $container->findDefinition(CompositeMessageParser::class);

        foreach ($container->findTaggedServiceIds(self::TAG_MESSAGE_BROKER_MESSAGE_PARSER) as $id => $tags) {
            $serviceDefinition = $container->getDefinition($id);
            //don't register composite message parser to itself
            if ($serviceDefinition->getClass() === CompositeMessageParser::class) {
                continue;
            }

            $compositeParser->addMethodCall('registerParser', [new Reference($id)]);
        }
    }
}
