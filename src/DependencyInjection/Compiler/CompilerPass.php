<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerBundle\DependencyInjection\Compiler;

use Predis\ClientInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Talentry\MessageBroker\ApplicationInterface\FailedMessageSpool;
use Talentry\MessageBroker\Infrastructure\FailedMessageSpool\RedisFailedMessageSpool;

class CompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if ($container->hasAlias(ClientInterface::class)) {
            $container->setAlias(FailedMessageSpool::class, RedisFailedMessageSpool::class);
        }
    }
}
