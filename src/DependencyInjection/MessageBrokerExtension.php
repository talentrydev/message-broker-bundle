<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Talentry\MessageBroker\ApplicationInterface\MessageParser;
use Talentry\MessageBroker\ApplicationInterface\MessageSerialization\MessageSerializationStrategy;
use Talentry\MessageBroker\Infrastructure\MessageBrokerFactory;
use Talentry\MessageBrokerBundle\DependencyInjection\Compiler\RegisterChannelsCompilerPass;
use Talentry\MessageBrokerBundle\DependencyInjection\Compiler\RegisterMessageParsersCompilerPass;

class MessageBrokerExtension extends Extension
{
    /**
     * @param array<mixed> $configs
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('parameters.yml');
        $loader->load('services.yml');

        $configuration = $this->processConfiguration(new Configuration(), $configs);

        $messageBrokerFactory = $container->getDefinition(MessageBrokerFactory::class);
        $messageBrokerFactory->setArgument('$publisherImplementation', $configuration['publisher_implementation']);
        $messageBrokerFactory->setArgument('$subscriberImplementation', $configuration['subscriber_implementation']);
        $messageBrokerFactory->setArgument('$namespace', $configuration['namespace']);
        $messageBrokerFactory->setArgument('$awsRegion', $configuration['aws_region']);
        $messageBrokerFactory->setArgument('$snsTopicArn', $configuration['sns_topic_arn']);
        $messageBrokerFactory->setArgument('$snsEndpoint', $configuration['sns_endpoint']);
        $messageBrokerFactory->setArgument('$sqsEndpoint', $configuration['sqs_endpoint']);

        if (isset($configuration['kafka'])) {
            $kafkaConfiguration = $configuration['kafka'];
            $messageBrokerFactory->setArgument('$kafkaBrokerList', $kafkaConfiguration['broker_list']);
            $messageBrokerFactory->setArgument('$kafkaAutoOffsetReset', $kafkaConfiguration['auto_offset_reset']);
            $messageBrokerFactory->setArgument('$kafkaEnablePartitionEof', $kafkaConfiguration['enable_partition_eof']);
            $messageBrokerFactory->setArgument('$kafkaEnableAutoCommit', $kafkaConfiguration['enable_auto_commit']);
            $messageBrokerFactory->setArgument('$kafkaLogLevel', $kafkaConfiguration['log_level']);
            $messageBrokerFactory->setArgument('$kafkaMaxPollIntervalMs', $kafkaConfiguration['max_poll_interval_ms']);
            $messageBrokerFactory->setArgument('$kafkaSessionTimeoutMs', $kafkaConfiguration['session_timeout_ms']);
        }

        $container->setParameter(
            RegisterChannelsCompilerPass::PARAMETER_CHANNELS_CONFIGURATION,
            $configuration['channels'],
        );

        $container
            ->registerForAutoconfiguration(MessageParser::class)
            ->addTag(RegisterMessageParsersCompilerPass::TAG_MESSAGE_BROKER_MESSAGE_PARSER);

        $container
            ->registerForAutoconfiguration(MessageSerializationStrategy::class)
            ->addTag(RegisterChannelsCompilerPass::TAG_MESSAGE_BROKER_SERIALIZATION_STRATEGY);
    }
}
