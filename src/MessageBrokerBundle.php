<?php

declare(strict_types=1);

namespace Talentry\MessageBrokerBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Talentry\MessageBrokerBundle\DependencyInjection\Compiler\CompilerPass;
use Talentry\MessageBrokerBundle\DependencyInjection\Compiler\RegisterMessageParsersCompilerPass;
use Talentry\MessageBrokerBundle\DependencyInjection\Compiler\RegisterChannelsCompilerPass;

class MessageBrokerBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new RegisterMessageParsersCompilerPass());
        $container->addCompilerPass(new RegisterChannelsCompilerPass());
        $container->addCompilerPass(new CompilerPass());
    }
}
